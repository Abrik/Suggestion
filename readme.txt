Задание 
===============================================================================================================
[HttpPost("get-suggestions"]
public async Task<GetSuggestResponse> GetSuggestions(GetSuggestionRequest request, CancellationToken token)
{
}

public class GetSuggestionRequest
{
	public string SearchString {get;set;}
}

public class GetSuggestResponse
{
	public List<string> Suggestions {get;set;}
	public List<double> Weights {get;set;}
}

1. В request получаем коды предложений через запятую (коды уникальны и не повторяются в строке).
2. По полученным кодам идем в БД и получаем идентификаторы предложений.
3. С данными идентификаторами предложений идем по HTTP в сторонний сервис и получаем веса предложений (double).
4. Возвращаем ответ.
5. Решить задачу оптимальным способом
6. Во внешний сервис можем сходить только с одним идентификатором
--------------------------------------------------------------------------------------------------------------



Решение, способ проверки
===============================================================================================================
Ссылка:
https://localhost:5001/suggestion/get-suggestions

Headers:
"Content-Type","application/json-patch+json"
"Accept":"text/plain"

Body:
{
  "SearchString": "a1,a2,a3,b1,b2,b3,c1,c20,c30"
}

Result:
{
    "suggestions": [
        "a1",
        "a2",
        "a3",
        "b1",
        "b2",
        "b3",
        "c1",
        "c20",
        "c30"
    ],
    "weights": [
        1.68,
        2.52,
        3.35,
        4.19,
        5.02,
        5.86,
        6.69,
        0,
        0
    ]
}

