﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SuggestionClient
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {

                int len = 5;
                var mas = new double[len];
                for (var i = 0; i < len; i++)
                {
                    mas[i] = StepAvg();
                }
                Console.WriteLine("\n\n");
                Console.WriteLine(string.Join(",\n", mas));
                Console.WriteLine("avg="+ mas.Average());

                Console.ReadKey();
            }
        }
        static double StepAvg()
        {
            int len = 1;
            var mas = new long[len];
            for (var i = 0; i < len; i++)
            {
                mas[i] = Step();
            }
            Console.WriteLine("avg=" + mas.Average());
            return mas.Average();
        }
        static long Step()
        {
            var timer = new Stopwatch();
            timer.Start();

            //Console.WriteLine("\n");
            getData().Wait();

            timer.Stop();
            Console.WriteLine(timer.ElapsedMilliseconds);
            return timer.ElapsedMilliseconds;
        }

        static async Task getData()
        {
            var genData = string.Join(",", Enumerable.Range(95_000, 1_000).Select(x => "a" + x));
            var requestData = "{\"SearchString\":\""+ genData + "\"}";
            var apiUrl = new Uri("http://localhost:5000/Suggestion/get-suggestions");

            var http = new HttpClient();
            HttpContent content = new StringContent(requestData, Encoding.UTF8, "application/json");
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = cancelTokenSource.Token;
            
            var response = await http.PostAsync(apiUrl, content,token);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                HttpContent responseContent = response.Content;
                var data = await responseContent.ReadAsStringAsync();
                //Console.WriteLine(data);
            }
            else
                Console.WriteLine(response);
        }
    }
}
