using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SuggestionSln.BI.ServiceInterfaces;
using SuggestionSln.BI.Services;
using SuggestionSln.Data;
using SuggestionSln.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuggestionSln.Instance
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IConfiguration configuration { get; }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowAll");
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            //DB
            services.AddScoped<DbContext, SuggestionDbContext>();
            services.AddDbContext<SuggestionDbContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("SuggestionDBstring")));

            //services
            services.AddScoped<ISuggestionService, SuggestionService>();
            services.AddScoped<ISuggestionWeightService, SuggestionWeightService>();            
            services.AddScoped<ISuggestionRepository, SuggestionRepository>();
            services.AddScoped<IHttpClientService, HttpClientService>();
            services.AddScoped<ILog, Log>();

            services.AddControllers();
        }
    }

}
