﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.WebApi.Response
{
    public class GetSuggestResponse
    {
        public List<string> Suggestions { get; set; }
        public List<double> Weights { get; set; }
    }
}
