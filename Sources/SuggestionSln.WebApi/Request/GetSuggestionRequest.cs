﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.WebApi.Request
{
    public class GetSuggestionRequest
    {
        public string SearchString { get; set; }
    }
}
