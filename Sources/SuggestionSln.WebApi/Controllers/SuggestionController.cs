﻿using Microsoft.AspNetCore.Mvc;
using SuggestionSln.BI.ServiceInterfaces;
using SuggestionSln.WebApi.Request;
using SuggestionSln.WebApi.Response;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SuggestionSln.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SuggestionController : ControllerBase
    {
        private readonly ISuggestionService _suggestionService;
        private readonly ILog _log;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="SuggestionController"/>.
        /// </summary>
        /// <param name="suggestiontService">Сервис весов</param>

        public SuggestionController(ISuggestionService suggestionService,ILog log)
        {
            _suggestionService = suggestionService;
            _log = log;
        }


        [HttpPost("get-suggestions")]
        public async Task<IActionResult> GetSuggestions(GetSuggestionRequest request, CancellationToken token)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.SearchString))
                {
                    _log.Warning("SearchString is null");
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);
                }
                var timer = new Stopwatch();
                timer.Start();

                var codeList = request.SearchString.Split(',').ToHashSet<string>();
                var infoList = await _suggestionService.GetInfo(codeList);

                var response = new GetSuggestResponse()
                {
                    Suggestions = infoList.Keys.ToList(),
                    Weights = infoList.Values.Select(x=>x.Weight).ToList()
                };

                timer.Stop();
                Console.WriteLine(timer.ElapsedMilliseconds);

                return this.Ok(response);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
