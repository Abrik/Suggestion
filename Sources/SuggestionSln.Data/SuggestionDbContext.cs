﻿using Microsoft.EntityFrameworkCore;
using SuggestionSln.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuggestionSln.Data
{
    /// <summary>
    /// Контекст базы данных Suggestion.
    /// </summary>
    public class SuggestionDbContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="SuggestionDbContext"/>.
        /// </summary>
        public SuggestionDbContext(DbContextOptions<SuggestionDbContext> option) : base(option)
        {
            Database.EnsureCreated();
        }
        public DbSet<Suggestion> Suggestions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Suggestion>().HasData(
                Enumerable.Range(1, 100_000).Select(x =>
                new Suggestion(x, "a" + x)
            ));
        }
    }
}
