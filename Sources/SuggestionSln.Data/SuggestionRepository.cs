﻿using SuggestionSln.Data.DTO;
using SuggestionSln.Data.Entity;
using SuggestionSln.Data.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace SuggestionSln.Data
{
    public class SuggestionRepository : ISuggestionRepository
    {
        private readonly SuggestionDbContext context;

        public SuggestionRepository(SuggestionDbContext context)
        {
            this.context = context;
        }

        public IDictionary<string, SuggestionInfoDto> GetInfoList(HashSet<string> codeList)
        {
            return context.Suggestions.Where(x => codeList.Contains(x.Code))
                .ToDictionary(x => x.Code, x => new SuggestionInfoDto(x.Id));
        }
    }
}
