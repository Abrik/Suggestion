﻿using SuggestionSln.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.Data.Interfaces
{
    public interface ISuggestionRepository
    {
        IDictionary<string, SuggestionInfoDto> GetInfoList(HashSet<string> codeList);
    }
}
