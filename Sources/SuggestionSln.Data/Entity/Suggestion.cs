﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.Data.Entity
{
    public class Suggestion
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Suggestion"/>.
        /// </summary>
        /// <param name="id">Идентификатор предложения</param>
        /// <param name="code">Код предложения</param>
        /// </summary>
        public Suggestion(int id, string code)
        {
            Id = id;
            Code = code;
        }
        public Suggestion()
        {
            
        }

        /// <summary>
        /// Идентификатор предложения
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Код предложения
        /// </summary>
        public string Code { get; set; }
    }
}
