﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SuggestionSln.Data.DTO.Web
{
    public class ResponseModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Content { get; set; }

    }
}
