﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.Data.DTO
{
    public class SuggestionInfoDto
    {
        public int Id { get; set; }
        public double Weight { get; set; }

        public SuggestionInfoDto(int id)
        {
            Id = id;
        }
    }
}
