﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.BI.ServiceInterfaces
{
    public interface ILog
    {
        void Info(string Msg);
        void Warning (string Msg);
        void Error(string Msg);
    }
}
