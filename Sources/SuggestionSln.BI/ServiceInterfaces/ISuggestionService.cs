﻿
using SuggestionSln.Data.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuggestionSln.BI.ServiceInterfaces
{
    public interface ISuggestionService
    {
        Task<IDictionary<string, SuggestionInfoDto>> GetInfo(HashSet<string> odeList);
    }
}
