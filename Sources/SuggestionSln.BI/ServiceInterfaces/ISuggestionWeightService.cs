﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuggestionSln.BI.ServiceInterfaces
{
    public interface ISuggestionWeightService
    {
        Task<double> Get(int id);
        Task<double> GetMock(int id);
    }
}
