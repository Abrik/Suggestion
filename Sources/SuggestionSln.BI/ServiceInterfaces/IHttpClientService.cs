﻿using SuggestionSln.Data.DTO.Web;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuggestionSln.BI.ServiceInterfaces
{
    public interface IHttpClientService
    {
        Task<ResponseModel> Get(string uri);
    }
}
