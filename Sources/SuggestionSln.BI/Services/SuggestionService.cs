﻿using SuggestionSln.BI.ServiceInterfaces;
using SuggestionSln.Data.DTO;
using SuggestionSln.Data.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SuggestionSln.BI.Services
{
    public class SuggestionService : ISuggestionService
    {
        private readonly ISuggestionRepository _repo;
        private readonly ISuggestionWeightService _weightService;
        private readonly ILog _log;
        private int _maxParallelm = Environment.ProcessorCount * 2;

        public SuggestionService(ISuggestionRepository repo, ISuggestionWeightService weightService, ILog log)
        {
            _repo = repo;
            _weightService = weightService;
            _log = log;
        }

        public async  Task<IDictionary<string, SuggestionInfoDto>> GetInfo(HashSet<string> suggestionCodeList)
        {
            var infoDic = _repo.GetInfoList(suggestionCodeList);

            var tasks = new Task[infoDic.Count];
            int i = 0;
            SemaphoreSlim semphore = new SemaphoreSlim(_maxParallelm, _maxParallelm);
            foreach (var el in infoDic)
            {
                await semphore.WaitAsync();
                try
                {
                    tasks[i] = AddToDictionaryWeight(el);
                    i++;
                }
                finally
                { 
                    semphore.Release(); 
                }
            }
            Task.WaitAll(tasks);

            return infoDic;
        }
        private async Task AddToDictionaryWeight( KeyValuePair<string, SuggestionInfoDto> pair)
        {
            pair.Value.Weight = await _weightService.GetMock(pair.Value.Id);            
        }

    }
}
