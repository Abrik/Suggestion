﻿using Newtonsoft.Json.Linq;
using SuggestionSln.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SuggestionSln.BI.Services
{
    public class SuggestionWeightService: ISuggestionWeightService
    {
        private readonly IHttpClientService _httpClient;
        private readonly ILog _log;
        string uri = "https://reqres.in/api/products/";

        public SuggestionWeightService(IHttpClientService httpClient, ILog log)
        {
            _httpClient = httpClient;
            _log = log;

        }

        public async Task<double> Get(int id)
        {
            try
            {
                var response = await _httpClient.Get($"{uri}{id}");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    JObject jsonObj = JObject.Parse(response.Content);
                    double.TryParse(jsonObj["data"]["year"].ToString(),out var year);
                    return Math.Round(id *1679.1 / year, 2);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
            return 0;
        }
        public async Task<double> GetMock(int id)
        {
            var random = new Random();
            return random.Next(1, 100) / 100;
        }
    }
}
