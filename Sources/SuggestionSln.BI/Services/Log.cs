﻿using SuggestionSln.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuggestionSln.BI.Services
{
    public class Log : ILog
    {
        private int maxLenType = 7;
        private void LogWrite(string kindName, string Msg)
        {
            Console.WriteLine(
                DateTime.Now.ToString("hh:mm:ss.fff | ")
                + kindName.PadRight(maxLenType, ' ') + " | " 
                + Msg);
        }

        public void Error(string Msg)
        {
            LogWrite("Error", Msg);
        }

        public void Info(string Msg)
        {
            LogWrite("Info", Msg);
        }

        public void Warning(string Msg)
        {
            LogWrite("Warning", Msg);
        }
    }
}
