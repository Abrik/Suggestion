﻿using SuggestionSln.BI.ServiceInterfaces;
using SuggestionSln.Data.DTO.Web;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuggestionSln.BI.Services
{
    public class HttpClientService : IHttpClientService
    {
        private readonly HttpClient _httpClient;

        public HttpClientService()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public async Task<ResponseModel> Get(string url)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            var response = await _httpClient.SendAsync(request);
            var responseContent = response.Content;
            var content = await responseContent.ReadAsStringAsync();
            return new ResponseModel()
            {
                StatusCode = response.StatusCode,
                Content = content
            };
        }
    }
}
